{ program 4.9 from Jensen & Wirth       -- file graph1.pas }

program graph1(output);
const d = 0.0625; {1/16, 16 lines for interval [x,x+1]}
      s = 32; {32 character widths for interval [y,y+1]}
      h = 34; {character position of x-axis}
      c = 6.28318; {2*pi}  lim = 32;
var x,y,a,b : real;  i,n : integer;
begin
   for i := 0 to lim do
      begin
         x := d*i;
         a := exp(-x);
         b := sin(c*x);
         y := a*b;
         n := round(s*y) + h;
         repeat write(' ');  n := n-1
         until n=0;
         writeln('*')
      end
end.
