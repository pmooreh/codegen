# ---------------- Beginning of Generated Code --------------------
        .file   "foo"
        .text
.globl graph1
        .type   graph1, @function
graph1:
.LFB0:
	.cfi_startproc
	pushq	%rbp              # save base pointer on stack
	.cfi_def_cfa_offset 16
	movq	%rsp, %rbp        # move stack pointer to base pointer
	.cfi_offset 6, -16
	.cfi_def_cfa_register 6
        subq	$64, %rsp 	  # make space for this stack frame
	movq	%rbx, %r9        # save %rbx (callee-saved) in %r9
# ------------------------- begin Your code -----------------------------
	movl	$0,%eax         	#  0 -> %eax
	movl	%eax,-32(%rbp)     	#  %eax -> i
.L2:
	movl	-32(%rbp),%eax     	#  i -> %eax
	movl	$32,%ecx         	#  32 -> %ecx
	subl	%ecx,%eax         	#  %eax - %ecx -> %eax
	cmpl	$0,%eax         	#  %eax compare 0 -> %eax
	jg	.L4 			#  jump if     >
	movsd	.LC5(%rip),%xmm1   	#  0.062500 -> %xmm1
	movl	-32(%rbp),%ecx     	#  i -> %ecx
	cvtsi2sd	%ecx,%xmm2    	#  float %ecx -> %xmm2
	mulsd	%xmm2,%xmm1         	#  %xmm1 * %xmm2 -> %xmm1
	movsd	%xmm1,-64(%rbp)     	#  %xmm1 -> x
	movsd	-64(%rbp),%xmm1     	#  x -> %xmm1
	movsd	.LC666(%rip),%xmm2   	#  0.000000 -> %xmm2
	xorpd	%xmm2,%xmm1           	#  negate %xmm1
	movsd	%xmm1,%xmm0         	#  %xmm1 -> %xmm0
	call	exp              	#  exp()
	movsd	%xmm0,%xmm1         	#  %xmm0 -> %xmm1
	movsd	%xmm1,-48(%rbp)     	#  %xmm1 -> a
	movsd	.LC6(%rip),%xmm1   	#  6.283180 -> %xmm1
	movsd	-64(%rbp),%xmm2     	#  x -> %xmm2
	mulsd	%xmm2,%xmm1         	#  %xmm1 * %xmm2 -> %xmm1
	movsd	%xmm1,%xmm0         	#  %xmm1 -> %xmm0
	call	sin              	#  sin()
	movsd	%xmm0,%xmm1         	#  %xmm0 -> %xmm1
	movsd	%xmm1,-40(%rbp)     	#  %xmm1 -> b
	movsd	-48(%rbp),%xmm1     	#  a -> %xmm1
	movsd	-40(%rbp),%xmm2     	#  b -> %xmm2
	mulsd	%xmm2,%xmm1         	#  %xmm1 * %xmm2 -> %xmm1
	movsd	%xmm1,-56(%rbp)     	#  %xmm1 -> y
	movl	$32,%ecx         	#  32 -> %ecx
	cvtsi2sd	%ecx,%xmm1    	#  float %ecx -> %xmm1
	movsd	-56(%rbp),%xmm2     	#  y -> %xmm2
	mulsd	%xmm2,%xmm1         	#  %xmm1 * %xmm2 -> %xmm1
	movsd	%xmm1,%xmm0         	#  %xmm1 -> %xmm0
	call	round              	#  round()
	movsd	%xmm0,%xmm1         	#  %xmm0 -> %xmm1
	movl	$34,%ecx         	#  34 -> %ecx
	cvtsi2sd	%ecx,%xmm2    	#  float %ecx -> %xmm2
	addsd	%xmm2,%xmm1         	#  %xmm1 + %xmm2 -> %xmm1
	cvttsd2si	%xmm1,%ecx    	#  fix %xmm1 -> %ecx
	movl	%ecx,-28(%rbp)     	#  %ecx -> n
.L0:
	movl	$.LC7,%edi       	#  addr of literal .LC7
	call	write              	#  write()
	movl	-28(%rbp),%ecx     	#  n -> %ecx
	movl	$1,%edx         	#  1 -> %edx
	subl	%edx,%ecx         	#  %ecx - %edx -> %ecx
	movl	%ecx,-28(%rbp)     	#  %ecx -> n
	movl	-28(%rbp),%ecx     	#  n -> %ecx
	movl	$0,%edx         	#  0 -> %edx
	subl	%edx,%ecx         	#  %ecx - %edx -> %ecx
	cmpl	$0,%ecx         	#  %ecx compare 0 -> %ecx
	jne	.L8 			#  jump if     !=
	jmp	.L1 			#  jump 
.L8:
	jmp	.L0 			#  jump 
.L1:
	movl	$.LC9,%edi       	#  addr of literal .LC9
	call	writeln              	#  writeln()
	movl	-32(%rbp),%edx     	#  i -> %edx
	movl	$1,%ebx         	#  1 -> %ebx
	addl	%ebx,%edx         	#  %edx + %ebx -> %edx
	movl	%edx,-32(%rbp)     	#  %edx -> i
	jmp	.L2 			#  jump 
.L4:
# ----------------------- begin Epilogue code ---------------------------
	movq	%r9, %rbx        # restore %rbx (callee-saved) from %r9
        leave
        ret
        .cfi_endproc
.LFE0:
        .size   graph1, .-graph1
# ----------------- end Epilogue; Literal data follows ------------------
        .section        .rodata
	.align 16
.LC666:                    # constant for floating negation
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.align  4
.LC7:
	.string	" "
	.align  4
.LC9:
	.string	"*"
	.align  8
.LC5:
	.long	0   	#  0.062500
	.long	1068498944
	.align  8
.LC6:
	.long	0   	#  6.283180
	.long	1075388922

        .ident  "CS 375 Compiler - Spring 2016"
