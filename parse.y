%{     /* pars1.y    Pascal Parser      Gordon S. Novak Jr.  ; 30 Jul 13   */

/* Copyright (c) 2013 Gordon S. Novak Jr. and
   The University of Texas at Austin. */

/* 14 Feb 01; 01 Oct 04; 02 Mar 07; 27 Feb 08; 24 Jul 09; 02 Aug 12 */

/*
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation; either version 2 of the License, or
; (at your option) any later version.

; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.

; You should have received a copy of the GNU General Public License
; along with this program; if not, see <http://www.gnu.org/licenses/>.
  */


/* NOTE:   Copy your lexan.l lexical analyzer to this directory.      */

       /* To use:
                     make pars1y              has 1 shift/reduce conflict
                     pars1y                   execute the parser
                     i:=j .
                     ^D                       control-D to end input

                     pars1y                   execute the parser
                     begin i:=j; if i+j then x:=a+b*c else x:=a*b+c; k:=i end.
                     ^D

                     pars1y                   execute the parser
                     if x+y then if y+z then i:=j else k:=2.
                     ^D

           You may copy pars1.y to be parse.y and extend it for your
           assignment.  Then use   make parser   as above.
        */

        /* Yacc reports 1 shift/reduce conflict, due to the ELSE part of
           the IF statement, but Yacc's default resolves it in the right way.*/

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include "token.h"
#include "lexan.h"
#include "symtab.h"
#include "parse.h"
#include "codegen.h"

        /* define the type of the Yacc stack element to be TOKEN */

#define YYSTYPE TOKEN
#define YYERROR_VERBOSE 1

TOKEN parseresult;

%}

/* Order of tokens corresponds to tokendefs.c; do not change */

%token IDENTIFIER STRING NUMBER   /* token types */

%token PLUS MINUS TIMES DIVIDE    /* Operators */
%token ASSIGN EQ NE LT LE GE GT POINT DOT AND OR NOT DIV MOD IN

%token COMMA                      /* Delimiters */
%token SEMICOLON COLON LPAREN RPAREN LBRACKET RBRACKET DOTDOT

%token ARRAY BEGINBEGIN           /* Lex uses BEGIN */
%token CASE CONST DO DOWNTO ELSE END FILEFILE FOR FUNCTION GOTO IF LABEL NIL
%token OF PACKED PROCEDURE PROGRAM RECORD REPEAT SET THEN TO TYPE UNTIL
%token VAR WHILE WITH


%%
    /* PROGRAM */
  program
    : PROGRAM IDENTIFIER LPAREN idlist RPAREN SEMICOLON block DOT
        { parseresult = makeProgram(cons($2, cons(makeprogn($3, $4), $7))); }
    ;
  idlist
    : IDENTIFIER COMMA idlist                 { $$ = cons($1, $3); }
    | IDENTIFIER                              { $$ = cons($1, NULL); }
    ;

    /* BLOCK */
  block
    : LABEL labelList SEMICOLON postLabel     { $$ = $4; }    
    | postLabel
    ;
  postLabel
    : CONST constantDeclarations postconst    { $$ = $3; }
    | postconst                               { $$ = $1; }
    ;
  postconst
    : TYPE typeDeclarations postType          { $$ = $3; }
    | postType
    ;
  postType
    : VAR varDeclarations postvar             { $$ = $3; }
    | postvar                                 { $$ = $1; }
    ;
  postvar
    : BEGINBEGIN statements END               { $$ = makeprogn($1, $2); }
    ;
    /* END BLOCK */

    /* BLOCK AUXILIARY */
  typeDeclarations
    : typeDeclaration typeDeclarations        { $$ = cons($1, $2); }
    | typeDeclaration                         { $$ = $1; }
    ;
  typeDeclaration
    : IDENTIFIER EQ type SEMICOLON            { $$ = installType($1, $3); }
    ;
  labelList
    : NUMBER COMMA labelList                  { $$ = cons($1, $3); }
    | NUMBER                                  { $$ = cons($1, NULL); }
  constantDeclarations
    : constantDeclaration constantDeclarations                
    | constantDeclaration                            
    ;
  constantDeclaration
    : IDENTIFIER EQ NUMBER SEMICOLON          { loadConstant($1, $3); }
    ;
  varDeclarations
    : varDeclaration varDeclarations                      
    | varDeclaration                                              
    ;
  varDeclaration
    : idlist COLON type SEMICOLON             { declareVars($1, $3); }
    ;
  type
    : simpletype                              { $$ = $1; }
    | POINT IDENTIFIER                        { $$ = instpoint($1, $2); }
    | PACKED typePostPacked                   { $$ = $2; }
    | typePostPacked                          { $$ = $1; }
    ;
  typePostPacked
    : ARRAY LBRACKET simpletypeList RBRACKET OF type { $$ = instarray($3, $6); }
    | RECORD fieldList END                    { $$ = instrec($2); }
    ;
  simpletypeList
    : simpletype COMMA simpletypeList         { $$ = cons($1, $3); }
    | simpletype                              { $$ = cons($1, NULL); }
    ;
  simpletype
    : IDENTIFIER                              { $$ = findType($1); }
    | LPAREN idlist RPAREN                    { $$ = makeEnum($2); }
    | constant DOTDOT constant                { $$ = makeSubRange($1, $3); }
    ;
  fieldList
    : typeList SEMICOLON fieldList            { $$ = cons($1, $3); }
    | typeList                                { $$ = cons($1, NULL); }
    | casePart
    ;
  typeList
    : idlist COLON type                       { $$ = groupType($3, $1); }
    | /* empty */
    ;
  casePart
    : CASE optionalId IDENTIFIER OF optionalCaseList
    ;
  optionalId
    : IDENTIFIER COLON
    ;
  optionalCaseList
    : caseList
    | /* empty */
    ;
  caseList
    : caseListEntry SEMICOLON optionalCaseList
    | caseListEntry
    ;
  caseListEntry
    : caseConstantList COLON LPAREN fieldList RPAREN
    ;
  caseConstantList
    : constant COMMA caseConstantList
    | constant
    ;
  constant
    : STRING
    | sign constantPostSign
    | constantPostSign
    ;
  constantPostSign
    : IDENTIFIER
    | NUMBER
    ;
    /* END BLOCK AUXILIARY */

  statements
    : statement SEMICOLON statements          { $$ = cons($1, $3); }
    | statement                               { $$ = cons($1, NULL); }
    ;
  statement
    : NUMBER COLON stmntPostLabel             { $$ = insertLabel($1, $3); }
    | stmntPostLabel
    ;
  stmntPostLabel
    : assignment                              { $$ = $1; }
    | funccall                                { $$ = $1; }
    | BEGINBEGIN statements END               { $$ = makeprogn($1, $2); }
    | IF expression THEN statement optElse    { $$ = makeif($1, $2, $4, $5); }
    | WHILE expression DO statement           { $$ = makeWhile($2, $4); }
    | REPEAT statements UNTIL expression      { $$ = makerepeat($2, $4); }
    | FOR assignment direction expression DO statement
        //{ $$ = makeprogn($1, cons($2, cons($4, $6))); }
        { $$ = makeFor($2, $3, $4, $6); }
    | GOTO NUMBER                             { $$ = makegoto($2); }
    ;
  optElse
    : ELSE statement
    | /* empty */                             { $$ = NULL; }
    ;
  funccall
    : IDENTIFIER LPAREN args RPAREN           { $$ = makeFuncCall($1, $3); }
    ;
  args
    : arg COMMA args                          { $$ = cons($1, $3); }
    | arg                                     { $$ = cons($1, NULL); }
    ;
  arg
    : expression                              { $$ = $1; }
    | IDENTIFIER                              { $$ = typifyId($1); }
    ;
  direction
    : TO                                      { $$ = $1; }
    | DOWNTO                                  { $$ = $1; }
    ;
  assignment
    : variable ASSIGN expression              { $$ = binop($2, $1, $3); }
    ;
  variable
    : IDENTIFIER indexings                    { $$ = makeAref($1, $2); }
    | IDENTIFIER                              { $$ = typifyId($1); }
    ;
  indexings
    : index indexings                         { $$ = cons($1, $2); }
    | index                                   { $$ = cons($1, NULL); }
    ;
  index
    : LBRACKET expressionList RBRACKET        { $$ = $2; }
    | DOT IDENTIFIER                          { $$ = $2; }
    | POINT                                   { $$ = $1; }
    ;
  expressionList
    : expression COMMA expressionList         { $$ = cons($1, $3); }
    | expression                              { $$ = cons($1, NULL); }
    ;
  expression
    : simpleExp relation simpleExp            { $$ = binop($2, $1, $3); }
    | simpleExp                               { $$ = $1; }
    ;
  relation
    : EQ
    | NE
    | GT
    | LT
    | GE
    | LE
    | IN
    ;
  simpleExp
    : sign simpleExpPostSign                  { $$ = unaryop($1, $2); }
    | simpleExpPostSign                       { $$ = $1; }
    ;
  simpleExpPostSign
    : simpleExpPostSign sign term             { $$ = binop($2, $1, $3); }
    | term                                    { $$ = $1; }
    ;
  sign
    : PLUS
    | MINUS
    ;
  term
    : term TIMES factor                       { $$ = binop($2, $1, $3); }
    | factor
    ;
  factor
    : unsignedConstant
    | variable
    | funccall                                { $$ = $1; }
    | LPAREN expression RPAREN                { $$ = $2; }
    ;
  unsignedConstant
    : NUMBER
    | NIL { $$ = makeIntegerToken(0); }
    | STRING
    ;
%%

/* You should add your own debugging flags below, and add debugging
   printouts to your programs.

   You will want to change DEBUG to turn off printouts once things
   are working.
  */

#define DEBUGALL 0   /* set bits here for debugging all  */
#define DB_PARSERES 0

#define DB_GROUPTYPE 0
#define DB_INSTALLTYPE 0
#define DB_INSTPOINT 0
#define DB_INSTREC 0
#define DB_INSTARRAY 0
#define DB_MAKEENUM 0
#define DB_MAKESUBRANGE 0
#define DB_MAKEGOTO 0
#define DB_INSTALLANDGETLABEL 0
#define DB_LOADCONSTANT 0
#define DB_INSERTLABEL 0
#define DB_GETCONSTANT 0
#define DB_DECLAREVARS 0
#define DB_MAKEAREF 0
#define DB_FINDTYPE 0
#define DB_TYPIFYID 0
#define DB_MAKEFOR 0
#define DB_MAKEREPEAT 0
#define DB_CONS 0
#define DB_BINOP 0
#define DB_MAKEFLOAT 0
#define DB_MAKEFIX 0
#define DB_UNARYOP 0
#define DB_MAKEWHILE 0
#define DB_MAKEIF 0
#define DB_MAKEPROGN 0
#define DB_MAKEFUNCCALL 0
#define DB_MAKEPROGRAM 0

int labelnumber = 0;  /* sequential counter for internal label numbers */
TOKEN constantsList = NULL;
TOKEN labelList = NULL;

   /*  Note: you should add to the above values and insert debugging
       printouts in your routines similar to those that are shown here.     */

TOKEN makeReservedToken(int which) {
  TOKEN tok = talloc();
  tok->tokentype = RESERVED;
  tok->datatype = INTEGER;
  tok->whichval = which;
  return tok;
}

TOKEN makeOperatorToken(int which) {
  TOKEN tok = talloc();
  tok->tokentype = OPERATOR;
  tok->datatype = INTEGER;
  tok->whichval = which;
  return tok;
}

TOKEN makeIntegerToken(int val) {
  TOKEN tok = talloc();
  tok->tokentype = NUMBERTOK;
  tok->datatype = INTEGER;
  tok->intval = val;
  return tok;
}


TOKEN groupType(TOKEN type, TOKEN idList) {
  if (DEBUGALL | DB_GROUPTYPE) { printf("GROUPING TYPE: %s\n", type->stringval); }
  type->operands = idList;
  return type;
}

TOKEN installType(TOKEN typeId, TOKEN type) {
  if (DEBUGALL | DB_INSTALLTYPE) { printf("INSTALLING TYPE: %s\n", typeId->stringval);}

  SYMBOL typeSym = searchins(typeId->stringval);
  typeSym->datatype = type->symtype;
  typeSym->kind = TYPESYM;
  typeSym->size = type->symtype->size;

  return typeId;
}

TOKEN instpoint(TOKEN tok, TOKEN typename) {
  if (DEBUGALL | DB_INSTPOINT) { printf("INSTALLING POINT to: %s\n", typename->stringval);}
  SYMBOL typeSym = searchins(typename->stringval);
  
  SYMBOL pointSym = symalloc();
  pointSym->kind = POINTERSYM;
  pointSym->datatype = typeSym;
  pointSym->size = 8;
  
  tok = talloc();
  tok->symtype = pointSym;

  return tok;
}

TOKEN instrec(TOKEN fieldList) {
  if (DEBUGALL | DB_INSTREC) { printf("INSTALLING RECORD\n");}

  SYMBOL recordSym = symalloc();
  recordSym->kind = RECORDSYM;

  //int offset = 0;
  int totalSize = 0;
  // go through each field list, get the length
  TOKEN currType = fieldList;
  SYMBOL prev = recordSym;

  while (currType != NULL) {
    SYMBOL type = searchins(currType->stringval);
    if (DEBUGALL | DB_INSTREC) { printf("    CURRENT TYPE: ");}
    dbprsymbol(type);

    int alignSize = alignsize(type);

    int overflow = totalSize % alignSize;
    if (overflow != 0) {
      totalSize += (alignSize - overflow);
    }

    TOKEN currId = currType->operands;

    while (currId != NULL) {
      SYMBOL idSym = makesym(currId->stringval);
      // set its datatype to the currType
      idSym->datatype = type;
      idSym->basicdt = type->basicdt;
      /*if (idSym->datatype->kind == )*/
      // set its offset
      idSym->size = type->size;
      idSym->offset = totalSize;

      if (DEBUGALL | DB_INSTREC) { printf("      CURRENT ID: %s\n", idSym->namestring);}
      if (DEBUGALL | DB_INSTREC) { printf("      size: %d; offset: %d\n", idSym->size, idSym->offset);}

      totalSize += idSym->size;

      prev->link = idSym;
      prev = idSym;
      currId = currId->link;
    }

    currType = currType->link;
  }

  recordSym->datatype = recordSym->link;
  recordSym->link = NULL;

  int overflow = totalSize % 16;
  if (overflow != 0) {
    totalSize += 16 - overflow;
  }

  recordSym->size = totalSize;

  fieldList->symtype = recordSym;

  return fieldList;
}

TOKEN instarray(TOKEN boundsList, TOKEN typetok) {
  if (DEBUGALL | DB_INSTARRAY) { printf("INSTALLING ARRAY, typetok has string val: %s\n", typetok->stringval);}

  SYMBOL bounds = boundsList->symtype;
  if (bounds == NULL) {
    bounds = searchst(boundsList->stringval)->datatype;
  }

  if (DEBUGALL | DB_INSTARRAY) { printf("    has bounds: %d to %d\n", bounds->lowbound, bounds->highbound);}

  if (boundsList->link != NULL) {
    if (DEBUGALL | DB_INSTARRAY) { printf("   GOING DEEPER INTO ARR\n");}
    typetok = instarray(boundsList->link, typetok);
  }

  SYMBOL typeSym = typetok->symtype;
  if (typeSym == NULL) {
    typeSym = searchst(typetok->stringval);
  }
  if (DEBUGALL | DB_INSTARRAY) { dbprsymbol(typeSym);}

  SYMBOL arrSym = symalloc();
  arrSym->kind = ARRAYSYM;
  arrSym->datatype = typeSym;
  arrSym->lowbound = bounds->lowbound;
  arrSym->highbound = bounds->highbound;
  arrSym->size = (arrSym->highbound - arrSym->lowbound + 1) * typeSym->size;

  if (DEBUGALL | DB_INSTARRAY) { printf("   ARR SIZE: %d\n", arrSym->size);}

  typetok = talloc();
  typetok->symtype = arrSym;

  return typetok;
}

TOKEN makeEnum(TOKEN enumIdList) {
  if (DEBUGALL | DB_MAKEENUM) { printf("MAKING ENUM\n");}

  TOKEN currTok = enumIdList;
  int currVal = 0;
  
  while (currTok != NULL) {
    TOKEN id = talloc();
    *id = *currTok;

    TOKEN num = makeIntegerToken(currVal);

    if (DEBUGALL | DB_MAKEENUM) { printf("    str: %s, val: %d\n", id->stringval, num->intval);}

    loadConstant(id, num);

    currVal++;
    currTok = currTok->link;
  }
  currVal--;

  SYMBOL enumSym = symalloc();
  enumSym->kind = SUBRANGE;
  enumSym->lowbound = 0;
  enumSym->highbound = currVal;
  enumSym->size = 4;

  enumIdList->symtype = enumSym;

  return enumIdList;
}

TOKEN makeSubRange(TOKEN low, TOKEN high) {
  if (DEBUGALL | DB_MAKESUBRANGE) { printf("MAKING SUBRANGE from %d to %d\n", low->intval, high->intval);}

  SYMBOL enumSym = symalloc();
  enumSym->kind = SUBRANGE;
  enumSym->lowbound = low->intval;
  enumSym->highbound = high->intval;
  enumSym->size = 4;

  low = talloc();
  low->symtype = enumSym;

  return low;
}

TOKEN makegoto(TOKEN labelNum) {
  if (DEBUGALL | DB_MAKEGOTO) { printf("MAKING GOTO\n");}
  TOKEN gotoTok = makeOperatorToken(GOTOOP);
  TOKEN labelTok = installAndGetLabel(labelNum->intval);

  labelNum->intval = labelTok->operands->intval;
  gotoTok->operands = labelNum;
  labelNum->link = NULL;

  return gotoTok;
}

TOKEN installAndGetLabel(int logicalLabelNum) {
  TOKEN currLabel = labelList;

  while (currLabel != NULL) {

    if (currLabel->tokentype == logicalLabelNum) {
      TOKEN num = makeIntegerToken(currLabel->intval);
      TOKEN labelOp = makeOperatorToken(LABELOP);
      labelOp->operands = num;
      return labelOp;
    }

    currLabel = currLabel->link;
  }

  // if here, label was not found
  TOKEN newLabel = talloc();
  newLabel->tokentype = logicalLabelNum;
  newLabel->intval = labelnumber++;

  labelList = cons(newLabel, labelList);

  TOKEN num = makeIntegerToken(newLabel->intval);
  TOKEN labelOp = makeOperatorToken(LABELOP);

  labelOp->operands = num;
  return labelOp;
}

TOKEN loadConstant(TOKEN id, TOKEN num) {
  id->operands = num;
  constantsList = cons(id, constantsList);
  
  if (DEBUGALL | DB_LOADCONSTANT) { 
    printf("loading constant\n");
    printf("identifier\n");
    dbugprinttok(id);
    printf("number\n");
    dbugprinttok(num);
    printf("\n");
  }
}

TOKEN insertLabel(TOKEN label, TOKEN statement) {
  TOKEN labelTok = installAndGetLabel(label->intval);
  TOKEN progn = makeOperatorToken(PROGNOP);
  progn->operands = labelTok;
  labelTok->link = statement;
  return progn;
}

TOKEN getConstant(char name[]) {
  TOKEN curr = constantsList;

  while (curr != NULL) {
    if (strcmp(name, curr->stringval) == 0) {
      return curr->operands;
    }
    curr = curr->link;
  }
}

/* FROM THE COURSE NOTES */
void declareVars(TOKEN idlist, TOKEN typetok) {
  if (DEBUGALL | DB_DECLAREVARS) { printf("DECLARING VARS ");}
  SYMBOL sym, typesym; int align;

  typesym = typetok->symtype;
  if (typesym == NULL) {
    typesym = searchst(typetok->stringval);
  }

  if (DEBUGALL | DB_DECLAREVARS) { printf(" with type: %s\n", typesym->namestring);}
  align = alignsize(typesym);

  while ( idlist != NULL )   /* for each id */
    {
      if (DEBUGALL | DB_DECLAREVARS) { printf("    DECLARING %s\n", idlist->stringval);}
      sym = insertsym(idlist->stringval);
      sym->kind = VARSYM;
      sym->offset = wordaddress(blockoffs[blocknumber], align);
      sym->size = typesym->size;
      blockoffs[blocknumber] = sym->offset + sym->size;
      sym->datatype = typesym;
      sym->basicdt = typesym->basicdt;
      idlist = idlist->link;
    };
}

TOKEN makeAref(TOKEN id, TOKEN indexList) {
  if (DEBUGALL | DB_MAKEAREF) { printf("AREF into: %s\n", id->stringval);}

  SYMBOL currDatatype = searchst(id->stringval)->datatype;
  if (DEBUGALL | DB_MAKEAREF) { dbprsymbol(currDatatype);}

  TOKEN base = id;

  TOKEN currIndex = indexList;

  while (currIndex != NULL) {
    if (DEBUGALL | DB_MAKEAREF) { printf("    CURRENT INDEX: ");}
    if (DEBUGALL | DB_MAKEAREF) { dbugprinttok(currIndex);}
    if (DEBUGALL | DB_MAKEAREF) { printf("     currDatatype: ");}
    if (DEBUGALL | DB_MAKEAREF) { dbprsymbol(currDatatype);}


    if (currIndex->tokentype == OPERATOR && currIndex->whichval == POINTEROP) {
      if (DEBUGALL | DB_MAKEAREF) { printf("        pointing to type: ");}
      SYMBOL pointType = currDatatype->datatype->datatype;
      if (DEBUGALL | DB_MAKEAREF) { dbprsymbol(pointType);}

      TOKEN point = makeOperatorToken(POINTEROP);
      point->operands = base;
      base = point;
      base->symtype = pointType;
      currDatatype = pointType;
    }
    else if (currDatatype->kind == ARRAYSYM) {
      if (DEBUGALL | DB_MAKEAREF) { printf("        into array with type: ");}
      SYMBOL arrType = currDatatype->datatype;
      int units = arrType->size;

      TOKEN arefTok = makeOperatorToken(AREFOP);
      TOKEN offsetTok = currIndex;

      if (currIndex->tokentype == NUMBERTOK) {
        int byteLoc = (currIndex->intval - currDatatype->lowbound) * units;
        if (DEBUGALL | DB_MAKEAREF) { printf("          integer, byteloc is %d\n", byteLoc);}
        offsetTok = makeIntegerToken(byteLoc);
      }
      if (currIndex->tokentype == IDENTIFIERTOK) {
        if (DEBUGALL | DB_MAKEAREF) { printf("          identifier, will construct index expr\n");}

        TOKEN diff = makeOperatorToken(MINUSOP);
        diff->operands = currIndex;
        currIndex->link = makeIntegerToken(currDatatype->lowbound);

        TOKEN times = makeOperatorToken(TIMESOP);
        times->operands = diff;
        diff->link = makeIntegerToken(units);


        offsetTok = times;
      }

      arefTok->operands = base;
      base->link = offsetTok;
      base = arefTok;
      base->symtype = arrType;
      currDatatype = arrType;
    }
    else if (currIndex->tokentype == IDENTIFIERTOK) {
      if (DEBUGALL | DB_MAKEAREF) { printf("        searching for field: %s\n", currIndex->stringval);}

      SYMBOL currField = currDatatype->datatype->datatype;
      if (currField->kind == RECORDSYM) {
        currField = currField->datatype;
      }
      SYMBOL correctField = NULL;

      while (correctField == NULL && currField != NULL) {
        if (DEBUGALL | DB_MAKEAREF) { printf("          current field is: ");}
        if (DEBUGALL | DB_MAKEAREF) { dbprsymbol(currField);}

        if (strcmp(currIndex->stringval, currField->namestring) == 0) {
          if (DEBUGALL | DB_MAKEAREF) { printf("              found it!\n");}
          correctField = currField;
        }

        currField = currField->link;
      }

      TOKEN arefTok = makeOperatorToken(AREFOP);
      TOKEN offsetTok = makeIntegerToken(correctField->offset);
      arefTok->operands = base;
      base->link = offsetTok;
      base = arefTok;
      base->symtype = correctField;
      currDatatype = correctField;
    }

    currIndex = currIndex->link;
  }

  return base;
}

TOKEN findType(TOKEN type) {
  return type;
}

TOKEN typifyId(TOKEN idTok) {
  if (DEBUGALL | DB_TYPIFYID) { printf("typing :"); dbugprinttok(idTok); }

  TOKEN constantTok = getConstant(idTok->stringval);
  if (constantTok != NULL) {
    idTok = talloc();
    *idTok = *constantTok;
  }
  else {
    idTok->datatype = searchst(idTok->stringval)->basicdt;
  }

  return idTok;
}

TOKEN makeFor(TOKEN assignment, TOKEN direction, TOKEN conditionExpr, TOKEN statement) {
  // label and goto tokens
  // it's okay that only one labelnum token exists
  TOKEN labelnum = makeIntegerToken(labelnumber++);

  TOKEN labelTok = makeOperatorToken(LABELOP);
  labelTok->operands = labelnum;

  TOKEN gotoTok = makeOperatorToken(GOTOOP);
  gotoTok->operands = labelnum;

  /* LOOP CONDITION */

  // get the counter
  TOKEN counter = talloc();
  *counter = *(assignment->operands);

  // get the correct comparator
  int whichCompare = LE - OPERATOR_BIAS;
  if (direction->whichval == (DOWNTO - RESERVED_BIAS)) {
    whichCompare = GE - OPERATOR_BIAS;
  }
  TOKEN comparator = makeOperatorToken(whichCompare);

  // // see if the condition expr is a constant
  // conditionExpr = typifyId(conditionExpr);

  // now link the counter to the expression, to comparator
  counter->link = conditionExpr;
  comparator->operands = counter;

  /* LOOP EXECUTION */

  // create an increment statement
  // make assign, plus ops
  TOKEN incrementTok = makeOperatorToken(ASSIGNOP);
  TOKEN plusTok = makeOperatorToken(PLUSOP);
  // duplicate the count for left and right side of assign
  TOKEN lhsCount = talloc();
  TOKEN rhsCount = talloc();
  *lhsCount = *counter;
  *rhsCount = *counter;
  // make a 1 token
  TOKEN one = makeIntegerToken(1);
  // form the assignment
  rhsCount->link = one;
  plusTok->operands = rhsCount;
  lhsCount->link = plusTok;
  incrementTok->operands = lhsCount;

  // link the statement, the increment, and goto together
  statement->link = incrementTok;
  incrementTok->link = gotoTok;

  // collect them in a progn
  TOKEN loopBody = makeprogn(talloc(), statement);

  /* IF */

  // create an if token
  TOKEN ifTok = makeOperatorToken(IFOP);
  // link loopBody to comparator, then all to the if
  comparator->link = loopBody;
  ifTok->operands = comparator;

  /* PUTTING IT ALL TOGETHER */

  // link the assignment, label, and if together
  assignment->link = labelTok;
  labelTok->link = ifTok;
  // group all into a progn. we're done!
  TOKEN finalLoop = makeprogn(talloc(), assignment);

  if (DEBUGALL | DB_MAKEFOR) { 
    printf("makeFor\n");
    printf("*** counter tok:     "); dbugprinttok(counter);
    printf("*** comparator:      "); dbugprinttok(comparator);
    printf("*** condition expr:  "); dbugprinttok(conditionExpr);
    dbugprinttok(assignment);
    dbugprinttok(assignment->operands);
    dbugprinttok(direction);
    dbugprinttok(conditionExpr);
    dbugprinttok(statement);
  }
  return finalLoop;
}

TOKEN makerepeat(TOKEN statements, TOKEN expr) {
  // label and goto tokens
  // it's okay that only one labelnum token exists

  TOKEN body = makeOperatorToken(PROGNOP);
  body->operands = statements;

  TOKEN bodyNum = makeIntegerToken(labelnumber++);
  TOKEN bodyLabel = makeOperatorToken(LABELOP);
  bodyLabel->operands = bodyNum;

  TOKEN endNum = makeIntegerToken(labelnumber++);
  TOKEN endLabel = makeOperatorToken(LABELOP);
  endLabel->operands = endNum;

  TOKEN bodyGoto = makeOperatorToken(GOTOOP);
  bodyGoto->operands = bodyNum;

  TOKEN endGoto = makeOperatorToken(GOTOOP);
  endGoto->operands = endNum;

  TOKEN ifTok = makeOperatorToken(IFOP);

  ifTok->operands = expr;
  expr->link = endGoto;

  bodyLabel->link = body;
  body->link = ifTok;
  ifTok->link = bodyGoto;
  bodyGoto->link = endLabel;

  TOKEN repeatCode = makeOperatorToken(PROGNOP);
  repeatCode->operands = bodyLabel;



  // TOKEN labelnum = makeIntegerToken(labelnumber++);

  // TOKEN labelTok = makeOperatorToken(LABELOP);
  // labelTok->operands = labelnum;

  // TOKEN gotoTok = makeOperatorToken(GOTOOP);
  // gotoTok->operands = labelnum;

  // // create an if token
  // TOKEN ifTok = makeOperatorToken(IFOP);

  // // link the expression, goto to if
  // ifTok->operands = expr;
  // expr->link = gotoTok;

  // // group the body statements into a progn
  // TOKEN body = makeprogn(talloc(), statements);

  // // now string all together
  // labelTok->link = body;
  // body->link = ifTok;

  // // and finally, collect in a progn
  // TOKEN repeatCode = makeprogn(talloc(), labelTok);

  if (DEBUGALL | DB_MAKEREPEAT) { 
    printf("makerepeat\n");
    dbugprinttok(expr);    
    dbugprinttok(body);
  }

  return repeatCode;
}

/* add item to front of list */
TOKEN cons(TOKEN item, TOKEN list) {
  item->link = list;
  if (DEBUGALL | DB_CONS) { 
       printf("cons\n");
       dbugprinttok(item);
       dbugprinttok(list);
  };
  return item;
}

/* reduce binary operator */
TOKEN binop(TOKEN op, TOKEN lhs, TOKEN rhs) {
  if (DEBUGALL | DB_BINOP) { 
    printf("IN BINOP\n");
    printf("  left: "); dbugprinttok(lhs);
    printf("  right: "); dbugprinttok(rhs); 
  }

  if (lhs->tokentype == IDENTIFIERTOK) {
    lhs = typifyId(lhs);
  }
  else if (lhs->tokentype == OPERATOR && lhs->whichval == AREFOP) {
    lhs->datatype = lhs->symtype->basicdt;
  }

  if (rhs->tokentype == IDENTIFIERTOK) {
    rhs = typifyId(rhs);
  }
  else if (rhs->tokentype == OPERATOR && rhs->whichval == AREFOP) {
    rhs->datatype = rhs->symtype->basicdt;
  }

  if (DEBUGALL | DB_BINOP) {
    printf(" left basicdt: %d\n", lhs->datatype);
    printf(" right basicdt: %d\n", rhs->datatype);
  }


  if (lhs->datatype != rhs->datatype) {
    if (op->whichval == ASSIGNOP) {
      if (DEBUGALL | DB_BINOP) {
        printf("   assignment\n");
      }
      if (lhs->datatype == INTEGER)
        rhs = makefix(rhs);
      else
        rhs = makefloat(rhs);
    }
    else {
      if (lhs->datatype == INTEGER) {
        lhs = makefloat(lhs);
      }
      else {
        rhs = makefloat(rhs);
      }
    }
  }

  op->operands = lhs;          /* link operands to operator       */
  lhs->link = rhs;             /* link second operand to first    */
  rhs->link = NULL;            /* terminate operand list */

  op->datatype = lhs->datatype;

  return op;
}

TOKEN makefloat(TOKEN tok) {
  TOKEN floatOP = makeOperatorToken(FLOATOP);
  floatOP->operands = tok;
  floatOP->datatype = REAL;
  return floatOP;
}

TOKEN makefix(TOKEN tok) {
  TOKEN fixOP = makeOperatorToken(FIXOP);
  fixOP->operands = tok;
  fixOP->datatype = INTEGER;
  return fixOP;
}

TOKEN unaryop(TOKEN op, TOKEN operand) {
  op->operands = operand;
  operand->link = NULL;
  op->datatype = operand->datatype;
  
  return op;
}

TOKEN makeWhile(TOKEN expr, TOKEN statement) {
  TOKEN labelTok = makeOperatorToken(LABELOP);
  TOKEN labelNum = makeIntegerToken(labelnumber++);
  labelTok->operands = labelNum;

  TOKEN gotoTok = makeOperatorToken(GOTOOP);
  gotoTok->operands = makeIntegerToken(labelNum->intval);

  TOKEN ifTok = makeOperatorToken(IFOP);
  TOKEN whileProgn = makeOperatorToken(PROGNOP);

  whileProgn->operands = labelTok;
  labelTok->link = ifTok;
  ifTok->operands = expr;
  expr->link = statement;
  statement->link = gotoTok;

  return whileProgn;
}

TOKEN makeif(TOKEN tok, TOKEN exp, TOKEN thenpart, TOKEN elsepart)
  {  tok->tokentype = OPERATOR;  /* Make it look like an operator   */
     tok->whichval = IFOP;
     if (elsepart != NULL) elsepart->link = NULL;
     thenpart->link = elsepart;
     exp->link = thenpart;
     tok->operands = exp;
    
    if (DEBUGALL | DB_MAKEIF) { 
    printf("makeif\n");
    dbugprinttok(tok);
    dbugprinttok(exp);
    dbugprinttok(thenpart);
    dbugprinttok(elsepart);
     return tok;
   }
 }

TOKEN makeprogn(TOKEN tok, TOKEN statements)
  {  tok->tokentype = OPERATOR;
     tok->whichval = PROGNOP;
     tok->operands = statements;
     if (DEBUGALL | DB_MAKEPROGN) {
      printf("makeprogn\n");
      dbugprinttok(tok);
      dbugprinttok(statements);
     };
     return tok;
   }

TOKEN makeFuncCall(TOKEN func, TOKEN args) {
  if (DEBUGALL | DB_MAKEFUNCCALL) { printf("FUNCALL\n");}
  SYMBOL funcSym = searchst(func->stringval);
  TOKEN funcCall = makeOperatorToken(FUNCALLOP);

  if (strcmp(funcSym->namestring, "new") == 0) {
    if (DEBUGALL | DB_MAKEFUNCCALL) { printf("  FUNC IS new(), on type: ");}
    SYMBOL newedType = searchst(args->stringval)->datatype->datatype->datatype;
    if (DEBUGALL | DB_MAKEFUNCCALL) { dbprsymbol(newedType);}

    
    TOKEN sizeTok = makeIntegerToken(newedType->size);
    funcCall->operands = func;
    func->link = sizeTok;

    TOKEN assignTok = makeOperatorToken(ASSIGNOP);
    assignTok->operands = args;
    args->link = funcCall;

    return assignTok;
  }
  else if ((strcmp(funcSym->namestring, "write") == 0) || strcmp(funcSym->namestring, "writeln") == 0) {
    if (DEBUGALL | DB_MAKEFUNCCALL) { printf("   FUNC is a write[ln](), arg has basic type: %d\n", args->datatype);}
    SYMBOL argType = args->symtype;

    int basicType = -1;

    if (args->tokentype == IDENTIFIERTOK) {
      if (DEBUGALL | DB_MAKEFUNCCALL) { printf("    this is an identifier\n");}
      argType = searchst(args->stringval);
    }

    if (argType != NULL) {
      if (DEBUGALL | DB_MAKEFUNCCALL) { printf("       more complicated, but has basic type: ");}
      SYMBOL basicTypeSym = argType->datatype;
      if (DEBUGALL | DB_MAKEFUNCCALL) { dbprsymbol(basicTypeSym);}

      basicType = basicTypeSym->basicdt;
    }
    else if (args->tokentype == STRINGTOK) {
      if (DEBUGALL | DB_MAKEFUNCCALL) { printf("      this is a string :)\n");}

      basicType = STRING;
    }

    funcCall->operands = func;
    func->link = args;

    if (strcmp(funcSym->namestring, "write") == 0) {
      if (basicType == INTEGER) {
        strcpy(func->stringval, "writei");
      }
      if (basicType == REAL) {
        strcpy(func->stringval, "writef");
      }
      if (basicType == STRING) {
        strcpy(func->stringval, "write");
      }
    }
    else {
      if (basicType == INTEGER) {
        strcpy(func->stringval, "writelni");
      }
      if (basicType == REAL) {
        strcpy(func->stringval, "writelnf");
      }
      if (basicType == STRING) {
        strcpy(func->stringval, "writeln");
      }
    }

    return funcCall;
  }
  else {
    func->link = args;
    funcCall->operands = func;

    funcCall->symtype = funcSym->datatype;
    funcCall->datatype = funcSym->datatype->basicdt;
    return funcCall;
  }
}

TOKEN makeProgram(TOKEN program) {
  // make a program op token
  TOKEN programTok = makeOperatorToken(PROGRAMOP);
  // link it to the program, and return!
  programTok->operands = program;

  return programTok;
}

int wordaddress(int n, int wordsize)
  { return ((n + wordsize - 1) / wordsize) * wordsize; }
 
yyerror(s)
  char * s;
  { 
  fputs(s,stderr); putc('\n',stderr);
  }

main() {
  int res;
  initsyms();
  res = yyparse();
 
  if (DB_PARSERES) {
    /* Pretty-print the result tree */
    printst();
    printf("yyparse result = %8d\n", res);
    dbugprinttok(parseresult);
    ppexpr(parseresult);
  }
  gencode(parseresult, blockoffs[blocknumber], labelnumber);
  }
