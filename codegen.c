/* codgen.c       Generate Assembly Code for x86         15 May 13   */

/* Copyright (c) 2013 Gordon S. Novak Jr. and The University of Texas at Austin
*/

/* Starter file for CS 375 Code Generation assignment.           */
/* Written by Gordon S. Novak Jr.                  */

/* This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License (file gpl.text) for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. */

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include "token.h"
#include "symtab.h"
#include "genasm.h"
#include "codegen.h"
#include "parse.h"

void genc(TOKEN code);

/* Set DEBUGGEN to 1 for debug printouts of code generation */
#define DEBUGGEN 0
#define DB_ASSIGN 0

#define LOWINTREG RBASE
#define HIGHINTREG 7

#define LOWFLOATREG (FBASE + 1)
#define HIGHFLOATREG (FMAX)

#define NUMINTREGS (HIGHINTREG - LOWINTREG + 1)
#define NUMFLOATREGS (HIGHFLOATREG - LOWFLOATREG + 1)

#define NUMOPS 4

int nextlabel;    /* Next available label number */
int stkframesize;   /* total stack frame size */

int intRegs[NUMINTREGS] = {0};
int floatRegs[NUMFLOATREGS] = {0};

int jumps[6] = {JNE, JE, JGE, JG, JL, JLE};
int ops[8] = {ADDL, SUBL, IMULL, DIVL, ADDSD, SUBSD, MULSD, DIVSD};


char dummyStr[] = {'t', 'e', 'm', 'p', 0};

/* Top-level entry for code generator.
pcode    = pointer to code:  (program foo (output) (progn ...))
varsize  = size of local storage in bytes
maxlabel = maximum label number used so far

Add this line to the end of your main program:
gencode(parseresult, blockoffs[blocknumber], labelnumber);
The generated code is printed out; use a text editor to extract it for
your .s file.
*/

void gencode(TOKEN pcode, int varsize, int maxlabel) {
    TOKEN name, code;
    name = pcode->operands;
    code = name->link->link;
    nextlabel = maxlabel + 1;
    stkframesize = asmentry(name->stringval,varsize);
    genc(code);
    asmexit(name->stringval);
}

void saveFloatRegs() {
    int i;
    for (i = LOWFLOATREG; i <= HIGHFLOATREG; i++) {
        asmstr(MOVSD, i, -8, RSP, dummyStr);
        asmimmed(SUBL, 8, RSP);
    }
}

void restoreFloatRegs() {
    int i;
    for (i = HIGHFLOATREG; i >= LOWFLOATREG; i--) {
        asmldr(MOVSD, 0, RSP, i, dummyStr);
        asmimmed(ADDL, 8, RSP);
    }
}

/* Trivial version: always returns RBASE + 0 */
/* Get a register.   */
/* Need a type parameter or two versions for INTEGER or REAL */
int getreg(int type) {
    if (type == WORD) {
        int i;
        for (i = 0; i < NUMINTREGS; i++) {
            if (intRegs[i] == 0) {
                intRegs[i] = 1;
                return i + LOWINTREG;
            }
        }
        return -1;
    }
    if (type == FLOAT) {
        int i;
        for (i = 0; i < NUMFLOATREGS; i++) {
            if (floatRegs[i] == 0) {
                floatRegs[i] = 1;
                return i + LOWFLOATREG;
            }
        }
        return -1;
    }
}

void unused(int reg) {
    if (reg >= LOWINTREG && reg <= HIGHINTREG) {
        intRegs[reg - LOWINTREG] = 0;
    }
    if (reg >= LOWFLOATREG && reg <= HIGHFLOATREG) {
        floatRegs[reg - LOWFLOATREG] = 0;
    }
}

/* Trivial version */
/* Generate code for arithmetic expression, return a register number */
int genarith(TOKEN code) {
    if (code == NULL) {
        return -1;
    }

    // need to chaeck the type

    int num = -1;
    int reg = -1;

    if (DEBUGGEN) {
        printf("genarith\n");
        dbugprinttok(code);
    };

    switch ( code->tokentype ) {
        case NUMBERTOK: {
            switch (code->datatype) {
                case INTEGER: {
                    num = code->intval;
                    reg = getreg(WORD);
                    if ( num >= MINIMMEDIATE && num <= MAXIMMEDIATE ) {
                        asmimmed(MOVL, num, reg);
                    }
                    return reg;
                    break;
                }
                case REAL: {
                    int floatReg = getreg(FLOAT);
                    int label = nextlabel++;
                    makeflit(code->realval, label);
                    asmldflit(MOVSD, label, floatReg);
                    return floatReg;
                    break;
                }
            }
            break;
        }
        case IDENTIFIERTOK: {
            SYMBOL sym = searchst(code->stringval);

            if (DEBUGGEN) { printf("need to load: "); dbprsymbol(sym); }

            switch (sym->basicdt) {
                case INTEGER: {
                    reg = getreg(WORD);
                    int netOffset = sym->offset - stkframesize;
                    asmld(MOVL, netOffset, reg, code->stringval);
                    return reg;
                    break;
                }
                case REAL: {
                    reg = getreg(FLOAT);
                    int netOffset = sym->offset - stkframesize;
                    asmld(MOVSD, netOffset, reg, code->stringval);
                    return reg;
                    break;
                }
            }
            break;
        }
        case OPERATOR: {
            switch (code->whichval) {
                case AREFOP: {
                    if (DEBUGGEN) printf("genarith aref\n");
                    int baseReg = genarith(code->operands);
                    int offReg = genarith(code->operands->link);

                    asmrr(ADDL, offReg, baseReg);
                    unused(offReg);

                    asmldr(MOVL, 0, baseReg, baseReg, dummyStr);
                    return baseReg;
                    break;
                }
                case POINTEROP: {
                    if (DEBUGGEN) printf("genarith point\n");
                    int pointReg = genarith(code->operands);
                    asmldr(MOVL, 0, pointReg, pointReg, dummyStr);
                    return pointReg;
                    break;
                }
                case FUNCALLOP: {
                    TOKEN function = code->operands;
                    TOKEN arg = function->link;
                    if (arg->datatype == INTEGER) {
                        int argReg = genarith(arg);
                        asmrr(MOVL, argReg, XMM0);
                        unused(argReg);
                        asmcall(function->stringval);
                        return EAX;
                    }
                    if (arg->datatype == REAL) {
                        int argReg = genarith(arg);
                        asmrr(MOVSD, argReg, XMM0);
                        //saveFloatRegs();
                        asmcall(function->stringval);
                        //restoreFloatRegs();
                        asmrr(MOVSD, XMM0, argReg);  
                        return argReg;
                    }                                  
                    break;
                }
                case FLOATOP: {
                    TOKEN lhs;
                    lhs = code->operands;

                    int lhsReg = genarith(lhs);

                    int floatReg = getreg(FLOAT);
                    asmfloat(lhsReg, floatReg);
                    unused(lhsReg);
                    return floatReg;
                    break;
                }
                case FIXOP: {
                    TOKEN lhs;
                    lhs = code->operands;

                    int lhsReg = genarith(lhs);

                    int fixedReg = getreg(WORD);
                    asmfix(lhsReg, fixedReg);
                    unused(lhsReg);
                    return fixedReg;
                    break;
                }
                case PLUSOP:
                case MINUSOP:
                case TIMESOP:
                case DIVIDEOP: {
                    TOKEN lhs, rhs;

                    int opIdx = code->whichval - PLUSOP;

                    lhs = code->operands;
                    rhs = lhs->link;

                    int lhsReg = genarith(lhs);
                    int rhsReg = genarith(rhs);

                    if (DEBUGGEN) printf("left reg: %d, right reg: %d\n", lhsReg, rhsReg);

                    // this is a legit binary operation
                    if (rhsReg != -1) {
                        int opOffset = 0;
                        if (code->datatype == REAL) {
                            opOffset = NUMOPS;
                        }
                        asmrr(ops[opIdx + opOffset], rhsReg, lhsReg);
                        unused(rhsReg);
                        return lhsReg;
                    }
                    if (rhsReg == -1 && code->whichval == MINUSOP) {
                        if (code->datatype == INTEGER) {
                            asmrr(NEGL, lhsReg, lhsReg);
                        }
                        else if (code->datatype == REAL) {
                            int extraReg = getreg(FLOAT);
                            asmfneg(lhsReg, extraReg);
                            unused(extraReg);
                        }
                        return lhsReg;
                    }
                    return lhsReg;
                    break;
                }
                case EQOP:
                case NEOP:
                case LTOP:
                case LEOP:
                case GEOP:
                case GTOP: {
                    TOKEN lhs, rhs;
                    lhs = code->operands;
                    rhs = lhs->link;

                    int lhsReg = genarith(lhs);
                    int rhsReg = genarith(rhs);

                    if (DEBUGGEN) printf("left reg: %d, right reg: %d\n", lhsReg, rhsReg);

                    asmrr(SUBL, rhsReg, lhsReg);
                    unused(rhsReg);
                    reg = lhsReg;
                    break;
                }
            }
            break;
        }
    }
}


/* Generate code for a Statement from an intermediate-code form */
void genc(TOKEN code) {
    TOKEN tok, lhs, rhs;
    int reg, offs;
    SYMBOL sym;

    if (DEBUGGEN) printf("genc\n");
    if (DEBUGGEN) dbugprinttok(code);

    if ( code->tokentype != OPERATOR ) {
        if (DEBUGGEN) printf("Bad code token");
        if (DEBUGGEN) dbugprinttok(code);
    }

    switch ( code->whichval ) {
        case PROGNOP: {
            tok = code->operands;
            while ( tok != NULL ) {
                if (DEBUGGEN) printf("prognop iter\n");
                genc(tok);
                tok = tok->link;
            };
            break;
        }
        case FUNCALLOP: {
            int labelNum = nextlabel++;
            makeblit(code->operands->link->stringval, labelNum);
            asmlitarg(labelNum, EDI);

            asmcall(code->operands->stringval);
            
            break;
        }
        case ASSIGNOP: {
            if (DEBUGGEN | DB_ASSIGN) printf("assign\n");
            lhs = code->operands;
            rhs = lhs->link;
            int rhsReg = genarith(rhs);              /* generate rhs into a register */
            if (DEBUGGEN | DB_ASSIGN) printf("finished genarith of rhs\n");

            switch (lhs->tokentype) {
                case IDENTIFIERTOK: {
                    sym = searchst(lhs->stringval);
                    
                    if (DEBUGGEN | DB_ASSIGN) printf("symbol table entry fetched\n");
                    if (DEBUGGEN | DB_ASSIGN) dbprsymbol(sym);
                    
                    offs = sym->offset - stkframesize;

                    switch (code->datatype) {
                        case INTEGER: {
                            asmst(MOVL, rhsReg, offs, lhs->stringval);
                            unused(rhsReg);
                        break;
                        }
                        case REAL: {
                            asmst(MOVSD, rhsReg, offs, lhs->stringval);
                            unused(rhsReg);
                            break;
                        }
                    }
                    break;
                }
                case OPERATOR: {
                    if (lhs->whichval == AREFOP) {
                        int baseReg = genarith(code->operands);
                        int offReg = genarith(code->operands->link);

                        asmrr(ADDL, baseReg, offReg);
                        unused(baseReg);

                        int movOp = MOVL;
                        if (code->datatype == REAL) {
                            movOp = MOVSD;
                        }

                        asmstr(movOp, rhsReg, 0, offReg, dummyStr);
                        unused(rhsReg);
                        unused(offReg);
                    }
                    break;
                }
                default: {
                    break;
                }
            }
            break;
        }
        case LABELOP: {
            if (DEBUGGEN) printf("label\n");
            asmlabel(code->operands->intval);
            break;
        }
        case IFOP: {
            if (DEBUGGEN) printf("gen if code\n");
            TOKEN condition = code->operands;
            TOKEN body = condition->link;

            // save the register num that will hold the result
            int condReg = genarith(condition);
            // prepare a label for this if statement
            int labelNum = nextlabel++;

            // jump to that label if condition is not satisfied
            // jumps = {JNE, JE, JGE, JG, JL, JLE}
            // token = {EQ , NE, LT , LE, GE, GT }

            asmimmed(CMPL, 0, condReg);
            asmjump(jumps[condition->whichval - EQOP], labelNum);

            // now execute the body
            genc(body);
            // and tack on the label at the end
            asmlabel(labelNum);
            break;
        }
        case GOTOOP: {
            if (DEBUGGEN) { printf("goto this label: "); dbugprinttok(code->operands); }
            asmjump(JMP, code->operands->intval);
            break;
        }
    }
}
